#include "memman.h" 

MMEMBLOCK** mmblock_ptr;
_msz mptr_sz = 0;
_msz mptr_pt = 0;
char mm_session = 0x0;

void mm_coredump()
{
	printf("mmblock_ptr: 0x%x\n", mmblock_ptr);
	printf("mptr_sz: %lu\n",mptr_sz);
	printf("mptr_pt: %lu\n",mptr_pt);
	printf("mm_session: %i\n",mm_session);
}

void mm_dumpptr()
{
	_msz x = 0;
	printf("---- mm_dumptr\n");
	for(x = 0; x < mptr_sz; x++)
	{
		printf("%lu 0x%x\n",x,mmblock_ptr[x]);
	}
	return;
}

int mm_init()
{
	srand( time( NULL ) );
	if(mm_session)
		return -1;
	mmblock_ptr = (MMEMBLOCK**)calloc(1,sizeof(MMEMBLOCK*));
	mptr_sz = 1;
	mptr_pt = 0;
	mm_session = 0x1;
	return 0;
}

_msz mm_inc_mmblock(_msz addsz)
{
	_msz oldsize = mptr_sz*sizeof(MMEMBLOCK*);
	_msz newsize = (mptr_sz+addsz)*sizeof(MMEMBLOCK*);
	MMEMBLOCK** mmblock_tmp = (MMEMBLOCK**)calloc(mptr_sz, sizeof(MMEMBLOCK**));
	memmove(mmblock_tmp,mmblock_ptr,oldsize);
	mmblock_ptr = (MMEMBLOCK**)realloc(mmblock_ptr, newsize);
	memset(mmblock_ptr,0x0,newsize);
	memmove(mmblock_ptr,mmblock_tmp,oldsize);
	free(mmblock_tmp);
	mptr_sz += addsz;
	return mptr_sz;
}

void* mm_change_bsize(void* _ptr, _msz size)
{
	_msz pos = 0;
	char bverify = 0;
	if(!_ptr)
		return 0x0;
	for(pos = 0; pos <= mptr_pt; pos++)
	{
		if(pos == mptr_pt)
			return 0x0;
		if(mmblock_ptr[pos])
		{
			if(mmblock_ptr[pos]->memh == _ptr)
			{
				break;
			}
		}
	}

	// Check memory integrity
	bverify = mm_verifyBlock(mmblock_ptr[pos], 1);
	#ifdef GS_DEBUG
	switch(bverify)
	{
		case -1:
			printf("mm_change_bsize - Block is NULL\n");
		break;
		case 0:
			printf("mm_change_bsize - CALL ON INVALID BLOCK\n");
			printf("%u %u %u\n", mmblock_ptr[pos]->mwordA,*(unsigned int*)((char*)mmblock_ptr[pos]->memh + size),
				mmblock_ptr[pos]->mwordB);
		break;
		case 1:
			printf("mm_change_bsize - Block is OK\n");
		break;
		case 0x0F:
			printf("mm_change_bsize - Block does not exist!\n");
		break;
	}
	#endif
	if(bverify != 0x1)
	{
		return NULL;
	}

	void* temp = calloc(1,size + sizeof(unsigned int));
	if(size < mmblock_ptr[pos]->size)
	{
		memmove(temp,_ptr,size);
	}else{
		memmove(temp,_ptr,mmblock_ptr[pos]->size);
	}
	free(mmblock_ptr[pos]->memh);
	mmblock_ptr[pos]->memh = temp;
	mmblock_ptr[pos]->size = size;
	mmblock_ptr[pos]->mwordA = rand();
	mmblock_ptr[pos]->mwordB = mmblock_ptr[pos]->mwordA;
	*(unsigned int*)((char*)mmblock_ptr[pos]->memh + size) = mmblock_ptr[pos]->mwordA;
	//memmove(((char*)mmblock_ptr[pos]->memh + size), &mmblock_ptr[pos]->mwordA, sizeof(unsigned int));
	return temp;
}

int mm_memcpy(void* _dest, void* _src, unsigned int sz)
{
	_msz pos_dest = 0;
	_msz pos_src = 0;
	char srcfnd = 0;
	char bverify = 0;

	if(!_dest || !_src || _dest == _src)
		return 0x0;
	for(pos_dest = 0; pos_dest <= mptr_pt; pos_dest++)
	{
		if(pos_dest == mptr_pt)
			return 0x0;
		if(mmblock_ptr[pos_dest])
		{
			if(mmblock_ptr[pos_dest]->memh == _src)
			{
				pos_src = pos_dest;
				srcfnd = 1;
			}
			if(mmblock_ptr[pos_dest]->memh == _dest)
			{
				break;
			}
		}
	}
	if(!srcfnd)
	{
		for(pos_src = 0; pos_src <= mptr_pt; pos_src++)
		{
			if(pos_src == mptr_pt)
				return 0x0;
			if(mmblock_ptr[pos_src])
			{
				if(mmblock_ptr[pos_src]->memh == _src)
				{
					break;
				}
			}
		}
	}

	if(sz > mmblock_ptr[pos_src]->size || sz > mmblock_ptr[pos_dest]->size)
		return 0x0;

	bverify = mm_verifyBlock(mmblock_ptr[pos_src], 1);
	#ifdef GS_DEBUG
	switch(bverify)
	{
		case -1:
			printf("mm_memcpy - Source Block is NULL\n");
		break;
		case 0:
			printf("mm_memcpy - CALL ON INVALID SOURCE BLOCK\n");
			printf("%u %u %u\n", mmblock_ptr[pos]->mwordA,*(unsigned int*)((char*)mmblock_ptr[pos]->memh + size),
				mmblock_ptr[pos]->mwordB);
		break;
		case 1:
			printf("mm_memcpy - Source Block is OK\n");
		break;
		case 0x0F:
			printf("mm_memcpy - Source Block does not exist!\n");
		break;
	}
	#endif
	if(bverify != 0x1)
	{
		return bverify;
	}

	memmove(mmblock_ptr[pos_dest]->memh, mmblock_ptr[pos_src]->memh, sz);

	bverify = mm_verifyBlock(mmblock_ptr[pos_dest], 1);
	#ifdef GS_DEBUG
	switch(bverify)
	{
		case -1:
			printf("mm_memcpy - Destination Block is NULL\n");
		break;
		case 0:
			printf("mm_memcpy - CALL ON INVALID DEST BLOCK\n");
			printf("%u %u %u\n", mmblock_ptr[pos]->mwordA,*(unsigned int*)((char*)mmblock_ptr[pos]->memh + size),
				mmblock_ptr[pos]->mwordB);
		break;
		case 1:
			printf("mm_memcpy - Destination Block is OK\n");
		break;
		case 0x0F:
			printf("mm_memcpy - Destination Block does not exist!\n");
		break;
	}
	#endif
	if(bverify != 0x1)
	{
		return bverify;
	}

	return 0x1;
}

int mm_free(void* _ptr)
{
	_msz newsize = (mptr_sz-1)*sizeof(MMEMBLOCK*);
	_msz pos = 0, second = 0;
	char skip = 0;
	if(!_ptr)
		return -1;
	for(pos = 0; pos <= mptr_pt; pos++)
	{
		if(pos == mptr_pt)
			return -2;
		if(mmblock_ptr[pos])
		{
			if(mmblock_ptr[pos]->memh == _ptr)
			{
				free(mmblock_ptr[pos]->memh);
				mmblock_ptr[pos]->memh = NULL;
				break;
			}
		}

	}

	if(mmblock_ptr[pos] != NULL)
	{
		free(mmblock_ptr[pos]);
	}
	MMEMBLOCK** mmblock_tmp = (MMEMBLOCK**)calloc(mptr_sz-1, sizeof(MMEMBLOCK**));
	for(second = 0; second < mptr_sz; second++)
	{
		if(second == pos)
		{
			skip++;
			continue;
		}
		if(!skip)
		{
			mmblock_tmp[second] = mmblock_ptr[second];
		}else{
			mmblock_tmp[second-1] = mmblock_ptr[second];
		}
	}
	mmblock_ptr = (MMEMBLOCK**)realloc(mmblock_ptr, newsize);
	memmove(mmblock_ptr,mmblock_tmp,newsize);
	free(mmblock_tmp);
	mptr_sz--;
	mptr_pt--;
	return 0;
}

void* mm_request(_msz size, MMEMBLOCK** mmh)
{
	if(size < 1)
	{
		return NULL;
	}
	MMEMBLOCK* block_hnd = (MMEMBLOCK*)calloc(1,sizeof(MMEMBLOCK));
	void* memblock = calloc(1, size + sizeof(unsigned int));
	block_hnd->mwordA = rand();
	block_hnd->mwordB = block_hnd->mwordA;
	*(unsigned int*)((char*)memblock + size) = block_hnd->mwordA;
	//memmove(((char*)memblock + size), &block_hnd->mwordA, sizeof(unsigned int));
	block_hnd->memh = memblock;
	block_hnd->size = size;
	mmblock_ptr[mptr_pt] = block_hnd;
	if(mmh != NULL)
		*mmh = block_hnd;
	mm_inc_mmblock(1);
	mptr_pt++;
	return memblock;
}

char mm_verifyBlock(void *_ptr, char is_memblockptr)
{
	_msz pos = 0;
	MMEMBLOCK *mmblk = (MMEMBLOCK*)_ptr;
	if(!_ptr)
		return -1;
	if(!is_memblockptr)
	{
		for(pos = 0; pos <= mptr_pt; pos++)
		{
			if(pos == mptr_pt)
				return 0x0F; // Invalid block (does not exist)
			if(mmblock_ptr[pos])
			{
				if(mmblock_ptr[pos]->memh == _ptr)
				{
					mmblk = mmblock_ptr[pos];
					break;
				}
			}
		}
	}

	// Verify that the block structure has not been tainted
	if(mmblk->mwordA != mmblk->mwordB)
		return 0x0; // Tainted block
	// Verify that the memory block has not been tainted
	if(*(unsigned int*)((char*)mmblk->memh + mmblk->size) != mmblk->mwordA)
		return 0x0; // Tainted block
	return 0x1; // Healthy block
}

int mm_end()
{
	_msz x = 0;
	for(x = 0; x < mptr_pt; x++)
		if(mmblock_ptr[x] != NULL)
		{
			if(mmblock_ptr[x]->memh != NULL)
				free(mmblock_ptr[x]->memh);
			free(mmblock_ptr[x]);
		}

	free(mmblock_ptr);
	mmblock_ptr = NULL;
	mm_session = 0x0;
	mptr_sz = 0;
	mptr_pt = 0;
	return 0;
}