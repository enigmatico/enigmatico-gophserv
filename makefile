all:
	gcc -c memman.c -o memman.o
	gcc -c utils.c -o utils.o
	gcc -c config.c -o config.o
	gcc -c fileserver.c -o fileserver.o
	gcc -c socket.c -o socket.o
	gcc -c main.c -o main.o
	gcc memman.o utils.o config.o fileserver.o socket.o main.o -O2 -o bin/gophserv -lpthread

debug:
	gcc -g -c memman.c -o memman.o
	gcc -g -c utils.c -o utils.o -DGS_DEBUG
	gcc -g -c config.c -o config.o -DGS_DEBUG
	gcc -g -c fileserver.c -o fileserver.o -DGS_DEBUG
	gcc -g -c socket.c -o socket.o -DGS_DEBUG
	gcc -g -c main.c -o main.o -DGS_DEBUG
	gcc -g memman.o utils.o config.o fileserver.o socket.o main.o -o bin/gophserv -lpthread

install:
	cp	bin/gophserv /usr/bin
	cp	man/gophserv.1	/usr/local/man/man1
	mkdir -p	/var/gophserv-www
	mkdir -p	/etc/gophserv
	cp -R	bin/public	/var/gophserv-www
	cp	gophserv.conf.template	/etc/gophserv/gophserv.conf

uninstall:
	rm	/usr/bin/gophserv
	rm -rf	/var/gophserv-www
	rm -rf	/etc/gophserv

clean:
	rm *.o
	rm bin/gophserv
