#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include "memman.h"
#include "config.h"

void printHex(char *buff, size_t len) {
	int i = 0, z = 0, rows = 0;
	printf("\n---HEX DUMP 0x%x %u---------------------------------------------------------------\n\n", buff, len);
	printf("0x%02x>> ", rows*16);
	for (int x = 0; x < len; x++)
	{
		printf("%02x", buff[x]);
		i++;
		if (i > 15 || x == len - 1)
		{
			printf("	 ");
			for (z = x - i + 1; z <= x; z++)
			{
				if (buff[z] > 0x1F && buff[z] < 0x7F)
					printf("%c", buff[z]);
				else
					printf(" ");

				if (z < x)
					printf(".");
			}
			i = 0;
			rows++;
			printf("\n");
			if (x != len - 1)
				printf("0x%02x>> ", rows);
		}
		else {
			printf(" ");
		}
	}
	printf("\n---END DUMP-----------------------------------------------------------------------------\n\n");
	return;
}

int printlog(const char* message, ...)
{
	if(!serverConfig.cf_logdir)
	{
		return 0; //Disabled
	}
	va_list args;
	va_start(args, message);

	char* fname = (char*)mm_request(30, NULL);
	char* dstime = (char*)mm_request(80, NULL);
	//char* msg = (char*)mm_request(8192, NULL);
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	sprintf(fname, "%s/%i-%i-%i.log", serverConfig.cf_logdir, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);
	strftime(dstime, 40, "$[%d/%m/%Y %H:%M:%S]", &tm);

	FILE* logfile = fopen(fname, "a+");
	if(!logfile)
	{
		syslog(LOG_ERR, "Could not open the log file %s\n", fname);
		mm_free(fname);
		mm_free(dstime);
		return -1;
	}

	fprintf(logfile, "%s ", dstime);
	vfprintf(logfile, message, args);
	fprintf(logfile, "\n");

	mm_free(fname);
	mm_free(dstime);
	//mm_free(msg);
	fclose(logfile);
	//va_end(args);
	return 0;
}

char** expand(char** ar, int len)
{
	//char** new = calloc(len+1, sizeof(char**));
	char** new = (char**)mm_request(len+1*sizeof(char**), NULL);
	if(ar)
	{
		memcpy(new, ar, sizeof(char**)*len);
		//free(ar);
		mm_free(ar);
	}
	return new;
}

void destroyparts(char** parts, int numparts)
{
	int x = 0;
	while(x < numparts)
	{
		if(parts[x])
			mm_free(parts[x]);
			//free(parts[x]);
		x++;
	}
	mm_free(parts);
	return;
}

char** explode(char* string, int strlength, char delimiter, int* nparts)
{
	int x = 0;
	int chars = 0;
	char** parts = (char**)0x0;
	int numparts = 0;
	int prev = 0;
	parts = (char**)mm_request(sizeof(char**),NULL);
	while(x < strlength)
	{
		if(string[x] == delimiter)
		{
			if(chars < 1)
			{
				x++;
				continue;
			}
			//parts = expand(parts,numparts);
			parts = (char**)mm_change_bsize(parts, numparts);
			//char* part = calloc(1,chars+1);
			char* part = (char*)mm_request(chars+1, NULL);
			memcpy(part, &string[prev],chars);
			//memcpy(parts+(numparts*sizeof(char*)), part, sizeof(char*));
			parts[numparts] = part;
			prev = x+1;
			chars = 0;
			numparts++;
			x++;
			continue;
		}

		chars++;
		x++;
	}

	if(chars > 0)
	{
		parts = expand(parts,numparts);
		//char* part = calloc(1,chars+1);
		char* part = (char*)mm_request(chars+1, NULL);
		memmove(part, &string[prev],chars);
		parts[numparts] = part;
		chars = 0;
		numparts++;
	}

	*nparts = numparts;
	return parts;
}

char* getPathExtension(char* path, int strl)
{
	int x = strl-1;
	while(x >= 0)
	{
		if(path[x] == '.')
		{
			//char* ext = calloc(1,strl-x+1);
			char* ext = (char*)mm_request(strl-x+1, NULL);
			memcpy(ext, &path[x+1], strl-x);
			return ext;
		}
		x--;
	}
	return (char*)0x0;
}

int stripClientRequest(char* fullRequest, char* gs_buf, int gs_maxbuflen)
{
	int x = 0, y = 0;
	char c = 0;

	if(!fullRequest || !gs_buf || gs_maxbuflen < 1)
	{
		return -1;
	}

	while(y < gs_maxbuflen)
	{
		gs_buf[y] = 0x0;
		y++;
	}

	while(x < gs_maxbuflen - 1)
	{
		c = fullRequest[x];
		if(c == '\r')
		{
			if(fullRequest[x+1] == '\n')
			{
				y = 0;
				// Fuck memmove
				while(y < x)
				{
					gs_buf[y] = fullRequest[y];
					y++;
				}
				return x-1;
			}
		}
		x++;
	}
	return x;
}
