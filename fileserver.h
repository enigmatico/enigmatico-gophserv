#ifndef __goph_files_serv__h__
#define __goph_files_serv__h__

#define GOPHER_T_DIRECTORY 	0
#define GOPHER_T_TEXT		1
#define GOPHER_T_BINARY		2

#define GOPHER_TEXT 		'0'
#define GOPHER_MENU 		'1'
#define GOPHER_CCSO 		'2'
#define GOPHER_ERROR 		'3'
#define GOPHER_BINHEX 		'4'
#define GOPHER_DOS 			'5'
#define GOPHER_UUEN 		'6'
#define GOPHER_TEXTSEARCH 	'7'
#define GOPHER_TELNET 		'8'
#define GOPHER_BIN 			'9'
#define GOPHER_MIRROR 		'+'
#define GOPHER_GIF 			'g'
#define GOPHER_IMAGE 		'I'
#define GOPHER_TELNET3270 	'T'

#define GOPHER_HTML			'h'
#define GOPHER_INFORMATION 	'i'
#define GOPHER_SOUND 		's'

#define GOPHER_MULTIPART 	'M'
#define GOPHER_INFOLINE 	'i'

#define GSR_NOTFOUND "3The requested resource was not found on this location"
#define GSR_NOTAVAIL "3The requested resource is not available"

int getType(char* ext);
int getFileType(char* ext);
char * gs_directoryList(char* path);

#endif