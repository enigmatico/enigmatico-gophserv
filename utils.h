#ifndef __GS_UTILS__H
#define __GS_UTILS__H
	void printHex(char *buff, size_t len);
	char** expand(char** ar, int len);
	void destroyparts(char** parts, int numparts);
	char** explode(char* string, int strlength, char delimiter, int* nparts);
	char* getPathExtension(char* path, int strl);
	int stripClientRequest(char* fullRequest, char* gs_buf, int gs_maxbuflen);
	int printlog(char* data, ...);
#endif
