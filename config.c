#include<syslog.h>
#include "gophserv.h"
#include "memman.h"
#include "config.h"
#include "utils.h"

/* There are a lot of things Valgrind doesnt like about this code, but I have
checked and most of them look like false positives. Plus there aren't any
memory leaks and it works fine.

Most of the things it complains are about "uninitialized memory" on places
that has been initialized by using 'calloc'. It probably doesn't like
array indexes on memory blocks and treat them as unitialized.

More testing is required. Just in case. It should work though. */

struct gs_config serverConfig;

void initConfig(char* fpath)
{
	/* Initialize and load the default configuration */
	memset(&serverConfig,0x0,sizeof(struct gs_config));
	serverConfig.cf_address = DEFAULT_S_ADDR;
	serverConfig.cf_bindtoaddress = DEFAULT_S_BINDTOADDRESS;
	serverConfig.cf_port = atoi(DEFAULT_S_PORT);
	serverConfig.cf_usedomain = DEFAULT_S_USEDOMAIN;
	serverConfig.cf_domain = DEFAULT_S_DOMAIN;
	serverConfig.cf_public = DIRECTORY_ROOT;
    serverConfig.cf_dirlist = DEFAULT_S_DIRLIST;
	serverConfig.cf_logdir = DEFAULT_S_LOGDIR;
	serverConfig.cf_socktimeout = DEFAULT_S_SOCKTIMEOUT;

	/* Try to load the configuration file. */
	syslog(LOG_INFO, "Loading configuration file: %s\n", fpath);
    	loadConfigFile(fpath);
	syslog(LOG_INFO, "Configuration loaded");
	return;
}

/* Set individual parameters */
void setParam(int param, char* val)
{
	if(!val)
	{
		return;
	}
	switch(param)
	{
		case GS_CFL_ADDRESS:
			serverConfig.cf_address = mm_request(strlen(val)+1,NULL);
			strcpy(serverConfig.cf_address, val);
		break;
		case GS_CFL_BINDTOADDRESS:
			serverConfig.cf_bindtoaddress = atoi(val);
		break;
		case GS_CFL_PORT:
			serverConfig.cf_port = atoi(val);
		break;
		case GS_CFL_USEDOMAIN:
			serverConfig.cf_usedomain = atoi(val);
		break;
		case GS_CFL_DOMAIN:
			serverConfig.cf_domain = mm_request(strlen(val)+1,NULL);
			strcpy(serverConfig.cf_domain, val);
		break;
		case GS_CFL_PUBLIC:
			serverConfig.cf_public = mm_request(strlen(val)+1,NULL);
			strcpy(serverConfig.cf_public, val);
		break;
        case GS_CFL_DIRLIST:
            serverConfig.cf_dirlist = atoi(val);
        break;
		case GS_CFL_LOGDIR:
			if(strlen(val) > 0)
			{
				serverConfig.cf_logdir = mm_request(strlen(val)+1,NULL);
				strcpy(serverConfig.cf_logdir, val);
			}else{
				serverConfig.cf_logdir = 0x0L;
			}
		break;
		case GS_CFL_SOCKTIMEOUT:
			serverConfig.cf_socktimeout = atoi(val);
		break;
	}
	return;
}

/* Process a text line from the config file */
int processConfigLine(char* line)
{
	int parts = 0, c = 0, x = 1, sl = 0;
	char procopt[256] = {0x0};
	char** bits = (char**)0x0;
	bits = explode(line, strlen(line),':', &parts);
	if(parts < 2 || !bits)
	{
		return -1;
	}

	/* Strip leading spaces and linefeeds */
	sl = strlen(bits[1]);
	while(x < sl && x < 255)
	{
		if(bits[1][x] != '\r' && bits[1][x] != '\n' && bits[1][x] != '\t')
		{
			procopt[x-1] = bits[1][x];
		}else{
			if(bits[1][x] != '\r' || bits[1][x] != '\n')
				break;
		}
		x++;
	}

	/* Ignore comments and sections */
	if(bits[0][0] == '#' && bits[0][0] == '[')
	{
		return 0;
	}

	/* Process each option */
	if(!strcmp(bits[0],"address"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_address = mm_request(strlen(procopt)+1,NULL);
		strcpy(serverConfig.cf_address, procopt);
	}
	if(!strcmp(bits[0],"bindtoaddress"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_bindtoaddress = atoi(procopt);
	}
	if(!strcmp(bits[0],"listen"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_port = atoi(procopt);
	}
	if(!strcmp(bits[0],"usedomain"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_usedomain = atoi(procopt);
	}
	if(!strcmp(bits[0],"domain"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_domain = mm_request(strlen(procopt)+1,NULL);
		strcpy(serverConfig.cf_domain, procopt);
	}
	if(!strcmp(bits[0],"directory_root"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_public = mm_request(strlen(procopt)+1,NULL);
		strcpy(serverConfig.cf_public, procopt);
	}
	if(!strcmp(bits[0],"directory_listing"))
    {
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_dirlist = atoi(procopt);
    }
	if(!strcmp(bits[0],"log_directory"))
	{
		if(strlen(bits[1])<1)
			return -1;
		if(strlen(procopt) > 0)
		{
			serverConfig.cf_logdir = mm_request(strlen(procopt)+1,NULL);
			strcpy(serverConfig.cf_logdir, procopt);
		}else{
			serverConfig.cf_logdir = 0x0L;
		}
	}
	if(!strcmp(bits[0],"socket_timeout"))
	{
		if(strlen(bits[1])<1)
			return -1;
		serverConfig.cf_socktimeout = atoi(procopt);
	}
	destroyparts(bits,parts);
	return 1;
}

/* Load a configuration file */
int loadConfigFile(char* fpath)
{
	FILE* cf;
	_msz fsize;
	char line[256] = {0x0};
	short bread = 0;

	if(!fpath)
	{
		syslog(LOG_INFO, "Configuration file not found. Looking for it on current directory...\n");
		cf = fopen("gophserv.conf", "r");
	}else{
		cf = fopen(fpath, "r");
	}
	if(!cf)
	{
		syslog(LOG_WARNING, "Error: The configuration file could not be loaded.\nDefaults loaded.\n");
		return -1;
	}

	fseek(cf, 0L, SEEK_END);
	fsize = ftell(cf);
	fseek(cf, 0L, SEEK_SET);

	while(bread = fgets(line,256,cf) != NULL)
	{
		processConfigLine(line);
		memset(line,0x0,256);
	}

	fclose(cf);
	return 0;
}
