#ifndef __CONFIG_H__11
#define __CONFIG_H__11

#ifndef GS_CFL_FLAGS
#define GS_CFL_FLAGS
	#define GS_CFL_ADDRESS			0
	#define GS_CFL_BINDTOADDRESS	1
	#define GS_CFL_PORT				2
	#define GS_CFL_USEDOMAIN		3
	#define GS_CFL_DOMAIN			4
	#define GS_CFL_PUBLIC			5
	#define GS_CFL_DIRLIST			6
	#define GS_CFL_LOGDIR			7
	#define GS_CFL_SOCKTIMEOUT		8
#endif

#ifndef DEFAULT_S_ADDR
	#define DEFAULT_S_ADDR "127.0.0.1"
#endif
#ifndef DEFAULT_S_BINDTOADDRESS
	#define DEFAULT_S_BINDTOADDRESS 0
#endif
#ifndef DEFAULT_S_PORT
	#define DEFAULT_S_PORT "70"
#endif
#ifndef DEFAULT_S_DOMAIN
	#define DEFAULT_S_DOMAIN "localhost"
#endif
#ifndef DEFAULT_S_USEDOMAIN
	#define DEFAULT_S_USEDOMAIN 0
#endif
#ifndef DIRECTORY_ROOT
	#define DIRECTORY_ROOT "public" //<-This is the path to your public folder
#endif
#ifndef DEFAULT_S_DIRLIST
	#define DEFAULT_S_DIRLIST	1
#endif
#ifndef DEFAULT_S_LOGDIR
	#define DEFAULT_S_LOGDIR	"./logs"
#endif
#ifndef DEFAULT_S_SOCKTIMEOUT
	#define DEFAULT_S_SOCKTIMEOUT	5
#endif

struct gs_config {
	char* cf_address;
	int cf_bindtoaddress;
	int cf_port;
	char cf_usedomain;
	char* cf_domain;
	char* cf_public;
    char cf_dirlist;
	char* cf_logdir;
	int cf_socktimeout;
};

extern struct gs_config serverConfig;

int loadConfigFile(char* fpath);
void initConfig(char* fpath);
void setParam(int param, char* val);

#endif
