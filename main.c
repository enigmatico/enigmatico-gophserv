#include<syslog.h>
#include "gophserv.h"
#include "config.h"
#include "socket.h"
#include "fileserver.h"
#include "utils.h"

int gs_sockserv;
const int true = 1;
int SERVER_PANIC = 0;
pthread_attr_t tattr;

/* Handle system signal interrupts. */
void signal_handler(int sig)
{
	switch(sig)
	{
		case SIGINT:
		gs_stop_server = 1;
		if(gs_sockserv > 0)
		{
			shutdown(gs_sockserv,2);
			close(gs_sockserv);
			syslog(LOG_ALERT, "SIGINT issued. Server stopped.\n");
			printlog("[signal_handler] SIGINT issued. Server stopped.");
		}
		break;
		case SIGHUP:
		gs_stop_server = 1;
		if(gs_sockserv > 0)
		{
			shutdown(gs_sockserv,2);
			close(gs_sockserv);
			syslog(LOG_ALERT, "SIGHUP issued. Server stopped.\n");
			printlog("[signal_handler] SIGHUP issued. Server stopped.");
		}
		break;
		case SIGTERM:
		gs_stop_server = 1;
		if(gs_sockserv > 0)
		{
			shutdown(gs_sockserv,2);
			close(gs_sockserv);
			syslog(LOG_ALERT, "SIGTERM issued. Server stopped.\n");
			printlog("[signal_handler] SIGTERM issued. Server stopped.");
		}
		break;
		case SIGSEGV:
			gs_stop_server = 1;
			if(gs_sockserv > 0)
			{
				shutdown(gs_sockserv,2);
				close(gs_sockserv);
			}
			mm_end();
			syslog(LOG_CRIT, "There was a BIG problem. Server shutdown. Abort!\n");
			printlog("[signal_handler] **CRITICAL** Segmentation fault. Aborted!");
			exit(1);
		break;
	}
}

int main(int args, char** argv)
{
		/* Vars and stuff... */
	int client_sock, c, x;
	struct sockaddr_in client;
	pthread_t gs_thread;

	/* Initializes the memory manager. */
	syslog(LOG_INFO, "Initializing memory...");
	mm_init();
	syslog(LOG_INFO, "Memory initialized.");

	/* Signals roll */
	syslog(LOG_INFO, "Setting up signal handlers...");
	signal(SIGINT, signal_handler);
	signal(SIGHUP, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);

	/* Initialize the configuration manager. */
	syslog(LOG_INFO, "Loading the configuration...");
	initConfig("/etc/gophserv/gophserv.conf");
	syslog(LOG_INFO, "Starting the server...");
	printlog("[main] SERVER STARTING");

	//Check for parameters
	for(x = 0; x < args; x++)
	{
		if(!strcmp(argv[x],"-a") || !strcmp(argv[x], "--address"))
		{
			if(x+1 >= args)
			{
				syslog(LOG_NOTICE, "Parameter --address takes 1 aditional parameter.");
				continue;
			}
			setParam(GS_CFL_ADDRESS, argv[x+1]);
		}

		if(!strcmp(argv[x],"-ba") || !strcmp(argv[x], "--bind-address"))
		{
			setParam(GS_CFL_BINDTOADDRESS, "1");
		}

		if(!strcmp(argv[x],"-p") || !strcmp(argv[x], "--port"))
		{
			if(x+1 >= args)
			{
				syslog(LOG_NOTICE, "Parameter --port takes 1 aditional parameter.");
				continue;
			}
			setParam(GS_CFL_PORT, argv[x+1]);
		}

		if(!strcmp(argv[x],"-ud") || !strcmp(argv[x], "--use-domain"))
		{
			setParam(GS_CFL_USEDOMAIN, "1");
		}

		if(!strcmp(argv[x],"-do") || !strcmp(argv[x], "--domain"))
		{
			if(x+1 >= args)
			{
				syslog(LOG_NOTICE, "Parameter --domain takes 1 aditional parameter.");
				continue;
			}
			setParam(GS_CFL_DOMAIN, argv[x+1]);
		}

		if(!strcmp(argv[x],"-r") || !strcmp(argv[x], "--root"))
		{
			if(x+1 >= args)
			{
				syslog(LOG_NOTICE, "Parameter --root takes 1 aditional parameter.");
				continue;
			}
			setParam(GS_CFL_PUBLIC, argv[x+1]);
		}
		if(!strcmp(argv[x],"-n") || !strcmp(argv[x], "--no-listing"))
        {
			setParam(GS_CFL_DIRLIST, "0");
        }
	}
	syslog(LOG_NOTICE, "Current log directory: ", serverConfig.cf_logdir);
	printlog("[main] Parameters processed.");

	#ifdef GS_DEBUG
		printf("address: %s\n",serverConfig.cf_address);
		printf("bindtoaddress: %i\n",serverConfig.cf_bindtoaddress);
		printf("port: %i\n",serverConfig.cf_port);
		printf("usedomain: %i\n",serverConfig.cf_usedomain);
		printf("domain: %s\n",serverConfig.cf_domain);
		printf("public: %s\n",serverConfig.cf_public);
		printf("directory_listing: %i\n", serverConfig.cf_dirlist);
		printf("log_directory: %i\n", serverConfig.cf_logdir);
	#endif

	c = sizeof(struct sockaddr_in);

	//Init pthread attributes. Must be detached from main process.
	pthread_attr_init(&tattr);
	pthread_attr_setdetachstate(&tattr,PTHREAD_CREATE_DETACHED);
	printlog("[main] pthread initialized.");

	//Start the server.
	gs_sockserv = gs_listenSocket();
	if(gs_sockserv < 0)
	{
		syslog(LOG_CRIT, "Error %i. See the logs (%s).\n",gs_sockserv, serverConfig.cf_logdir);
		printlog("[main] **ERROR** Error creating gs_sockserv %u (%u)",gs_sockserv, errno);
		mm_end();
		return -1;
	}
	setsockopt(gs_sockserv,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
	syslog(LOG_NOTICE, "Server started\n");

	listen(gs_sockserv,3);
	printlog("[main] --SERVER IS RUNNING--");

	//Main server loop
	while(!gs_stop_server)
	{
		// If the SERVER_PANIC flag is ON, stop everything!
		if(SERVER_PANIC != 0)
		{
			syslog(LOG_CRIT, "SERVER PANIC! (Check logs in %s)\n", serverConfig.cf_logdir);
			printlog("[main] --SERVER PANIC!--");
			break;
		}
		// Any pending connections?
		client_sock = accept(gs_sockserv, (struct sockaddr *)&client, (socklen_t*)&c);

		// If so, accept them
		if(client_sock != INVALID_SOCKET)
		{
			// Creates a connection thread to communicate with the client.
			pthread_create( &gs_thread, &tattr, gs_connectionThread, (void*) &client_sock);
		}else{
			#ifdef GS_DEBUG
				printf("Invalid socket. Connection not possible.\n");
			#endif
			printlog("[main] Invalid socket!");
		}
	}

	//Shutdown server and cleanup
	syslog(LOG_NOTICE,"Server shutting down...");
	shutdown(gs_sockserv,2);
	close(gs_sockserv);
	pthread_attr_destroy(&tattr);
	syslog(LOG_NOTICE, "SERVER SHUTDOWN\n");
	printlog("[main] --SERVER SHUTDOWN--");
	mm_end();
	printlog("[main] Memory freed");
	pthread_exit(0);
	printlog("[main] gophserv is now exiting.\n");
	syslog(LOG_NOTICE,"Process shutdown");
}
