# Gophserv
### A server for the Gopher Protocol

Use *Gophserv* as a server for the Gopher protocol. Gophserv follows the RFC 1436
standard from March of 1993. Gophserv can be used to host directories and HTML
pages.

When running gophserv, your gophserv.conf file must be inside a folder called
'gophservconf', located the same directory you are calling gophserv.conf from
(usually your current working directory in your command line prompt).

This project currently works on GNU Linux only. It also works on Windows under
Cygwin or a Linux subsystem.

### Instalation

    $ make

This will compile the current code and store the executable inside the bin/
folder. If you want to install it into your system, use:

    # make install

The executable will be copied into /usr/bin.

## Configuration

The server will look for a configuration file called 'gophserv.conf' inside /etc/gophservconf. The configuration file must be called gophserv.conf.

A template configuration file is issued with the source code.

### Run and stop the server

Make sure the public folder is in the right place (By default it is located at /var/gophserv-www). Then start your server from
the command line. Remember that starting the server in port 70 (default port)
requires root privileges.

    # gophserv
    
The server can be stopped by issuing an interrupt signal (Ctrl+C) at any moment.

### Program Options

* **-a --address**
Specify the IP address of the server (Example: -a 192.168.1.20).
* **-p --port**
Specify the TCP port to listen to (Example: -p 70).
* **-do --domain**
Specifies a domain for the server (Example: -do myserver.com)
* **-r --root**
Specify the location of the public directory (Example: -r /var/gopher/public)
* **-ba  --bind-address**
Binds the server to the given IP address instead of using INADDR_ANY (Flag)
* **-ud --use-domain**
Uses a domain for the directory listing menu entries. (Flag)
* **-n --no-listing**
Disable directory listing.

### Debug

There is a debug rule in the makefile for debugging the program.

    $ make debug

### Server content

Place your files and folders inside your public folder. Each folder can contain
a file called 'gophermap' with no extension, that might contain a raw gopher menu.
Remember to set your editor to use UNIX line feeds and tabs instead of spaces
when editing your gophermap files. Also I recommend giving your text lines a
maximum of 80 characters so it can fit in a standard terminal screen.

If no gophermap file is found, by default it will make a directory listing.
Files starting with a dot (.) are hidden and will not be listed.
