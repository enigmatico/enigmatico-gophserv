#ifndef __PRED_MESSAGES__H__
#define __PRED_MESSAGES__H__

#define MSG_HTTP_REQUEST "HTTP/1.0 501 Not Implemented\nServer: Enigmatico's Gopher Server\nContent-Length: 582\nContent-Type: text/html\n\n<html><head><title>Wrong protocol!</title></head><body><h1>THIS IS A GOPHER SERVER!</h1><br/><p>You are trying to access a GOPHER server using the HTTP protocol (i.e, you are connecting through a web browser using the HTTP protocol). Try using a GOPHER browser (Like LYNX) and make sure the protocol is set to 'gopher://'.</p><br/><p>This server is using <a href=\"https://gitlab.com/enigmatico/enigmatico-gophserv\">Enigmatico's Gopher server</a>. If you are a security researcher and you found a vulnerability, please commit an issue on the gitlab page. Thank you.</p></body></html>\n\n\0"

#endif
