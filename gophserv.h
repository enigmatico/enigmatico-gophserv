#ifndef __GOPHSERV__HEAD__
#define __GOPHSERV__HEAD__

typedef unsigned long int _msz;

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <error.h>
#include <signal.h>
#include "memman.h"

extern int SERVER_PANIC;

#endif
