#include "gophserv.h"
#include "utils.h"
#include "fileserver.h"
#include "socket.h"
#include "config.h"
#include "messages.h"
#include <sys/time.h>

unsigned char gs_stop_server = 0x0;

//Creates a listening socket for the server.
int gs_listenSocket()
{
	int socket_desc;
	struct sockaddr_in server;
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		perror("Can not create socket.\n");
		return -1;
	}
	server.sin_family = AF_INET;

	if(serverConfig.cf_bindtoaddress)
	{
		server.sin_addr.s_addr = inet_addr(serverConfig.cf_address);
	}else{
		server.sin_addr.s_addr = INADDR_ANY;
	}

	server.sin_port = htons(serverConfig.cf_port);
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		//print the error message
		if(errno == 13)
		{
			printf("You don't have enough privileges to listen on port %u.\nTry running the server with root privileges.\n", DEFAULT_S_PORT);
			return -1;
		}
		printf("Server bind failed for %s port %u. Error %u.\n", serverConfig.cf_address, serverConfig.cf_port, errno);
		return -1;
	}
	if(serverConfig.cf_bindtoaddress == 1)
	{
		printf("Binding server to %s port %u OK\n", serverConfig.cf_address, serverConfig.cf_port);
	}else{
		printf("Binding server to port %u OK\n", serverConfig.cf_port);
	}
	return socket_desc;
}

void *gs_connectionThread( void* ptr )
{
	int client_sock = *(int*) ptr, read_size = 0, write_size = 0;
	char *client_request = (char*)mm_request(8196, NULL);
	char *client_message = (char*)mm_request(8196, NULL);
	char *fileName = (char*)mm_request(4089, NULL);
	char *outBuf = (char*)mm_request(8192, NULL);
	char* dirlist;
	char flags = 1;
	FILE *ifile;
	int fsize = 0;
	int c = 0, y = 0, ps = 0, pl = 0;
	struct timeval tv;
	struct stat path_stat;

	tv.tv_sec = serverConfig.cf_socktimeout;
	tv.tv_usec = 0;

	setsockopt(client_sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
	read_size = recv(client_sock , client_message , 4088 , 0);

	if(errno != 0)
	{
		switch(errno)
		{
			case(EBADF):
				printlog("[gs_listenSocket] The socket argument for %u is not a valid file descriptor. ", client_sock);
				goto con_end;
			case(ECONNRESET):
				printlog("[gs_listenSocket] Connection %u closed by peer.", client_sock);
				goto con_end;
			case(EOPNOTSUPP):
				printlog("[gs_listenSocket] Supplied flags for %u not supported", client_sock);
				goto con_end;
			case(ETIMEDOUT):
				printlog("[gs_listenSocket] Connection with %u timed out after %i s", client_sock, serverConfig.cf_socktimeout);
				goto con_end;
			case(EIO):
				printlog("[gs_listenSocket] Connection with %u failed. I/O error.", client_sock);
				goto con_end;
			case(ENOBUFS):
				printlog("[gs_listenSocket] Connection with %u failed. ENOBUFS error.", client_sock);
				SERVER_PANIC = 1;
				goto con_end;
			case(ENOMEM):
				printlog("[gs_listenSocket] Connection with %u failed. Insuficient memory!", client_sock);
				SERVER_PANIC = 1;
				goto con_end;
		}
	}
	if(read_size == 0)
	{
		#ifdef GS_DEBUG
			printf("Client disconnected\n");
		#endif
		printlog("[gs_listenSocket] Client %u sent nothing. Disconnected.", client_sock);
		goto con_end;
	}else if(read_size == -1)
	{
		#ifdef GS_DEBUG
			printf("recv failed (%u) on fd %u\n", errno, client_sock);
		#endif
		printlog("[gs_listenSocket] recv failed (%u) on client %u", errno, client_sock);
		goto con_end;
	}

	printlog("[gs_listenSocket] New incomming connection at socket %u, got %u bytes.", client_sock, read_size);

	#ifdef GS_DEBUG
		printf("%u bytes received from the client\n", read_size);
		printHex(client_message, read_size);
	#endif

	int rSize = stripClientRequest(client_message, client_request, 4089);

	// Check if this is an HTTP request
	if(strlen(client_request) > 6)
	{
		if(strstr(client_request, " HTTP/1.") != 0x0L || strstr(client_request, " HTTP/2.") != 0x0L)
		{
			//strcpy(outBuf, MSG_HTTP_REQUEST, strlen(MSG_HTTP_REQUEST));
			write_size = send(client_sock, (const char*)MSG_HTTP_REQUEST, strlen(MSG_HTTP_REQUEST), 0);
			printlog("[gs_listenSocket] Received HTTP request from %u. Sent unsupported notice.", client_sock);
			goto con_end;
		}
	}

	char* ext = getPathExtension(client_request, rSize);
	printlog("[gs_listenSocket] Request from %u: %s", client_sock, client_request);

	sprintf(fileName, "%s%s", serverConfig.cf_public, client_request);

	if(ext)
	{
		flags = getFileType(ext);
	}else{
		flags = 0;
		stat(fileName, &path_stat);
		if(S_ISREG(path_stat.st_mode))
		{
			flags = 2;
		}
	}

	#ifdef GS_DEBUG
		printHex(client_message, read_size);
	#endif

	switch(flags)
	{
		case 0:
			// Directory listing.
			sprintf(fileName, "%s%s/gophermap",serverConfig.cf_public, client_request);

			#ifdef GS_DEBUG
				printf("Looking for %s...\n", fileName);
			#endif

			//Check if there is an index file. If so, serve it instead.
			if(access(fileName, F_OK) != -1)
			{
				
				ifile = fopen(fileName, "r");
				if(!ifile)
				{
					printlog("[gs_listenSocket] Could not open file %s (%u)\n", fileName, errno);
					goto con_end;
				}

				fseek(ifile, 0L, SEEK_END);
				fsize = ftell(ifile);
				fseek(ifile, 0L, SEEK_SET);
				#ifdef GS_DEBUG
					printf("Size: %u\nAt %u\n", fsize, ftell(ifile));
				#endif

				c = 0; y = 0; ps = 0; pl = 0;
				if(fsize > 0)
				{
					do
					{
						if(SERVER_PANIC != 0)
						{
							printlog("[gs_listenSocket] Server panic! Closing connection %u", client_sock);
							goto con_end;
						}
						c = fread((void*)outBuf, 1, 8192, ifile);
						
						#ifdef GS_DEBUG
							printf("Read %u bytes (%u)\n", c, errno);
						#endif

						if(c > 0)
						{
							write_size = send(client_sock, outBuf, c, 0);
							#ifdef GS_DEBUG
								printf("Written %u bytes out of %u.\n", write_size, c);
							#endif
						}
						y += c;
					}while(y < fsize && c > 0);
					printlog("[gs_listenSocket] Sent %u bytes from %s to client %u", y, fileName, client_sock);
				}

				fclose(ifile);

			}else{
				// Do the directory listing
				dirlist = gs_directoryList(client_request);
				if(!dirlist)
				{
					write_size = send(client_sock, GSR_NOTFOUND, strlen(GSR_NOTFOUND), 0);
					goto con_end;
				}
				write_size = send(client_sock, dirlist, strlen(dirlist), 0);
				#ifdef GS_DEBUG
					printf("Written %u bytes out of %u.\n", write_size, strlen(dirlist));
				#endif
				printlog("[gs_listenSocket] Sent %u bytes from %s to client %u", write_size, fileName, client_sock);
				mm_free(dirlist);
			}
		break;
		case 1:
			//Text file. Just serve it.
			#ifdef GS_DEBUG
				printf("Text File\n");
			#endif
			ifile = fopen(fileName, "r");
			if(!ifile)
			{
				#ifdef GS_DEBUG
					printf("Could not open file %s (%i)\n", fileName, errno);
				#endif
				printlog("Could not open file %s (%i)\n", fileName, errno);
				write_size = send(client_sock, GSR_NOTAVAIL, strlen(GSR_NOTAVAIL), 0);
				goto con_end;
			}
			fseek(ifile, 0L, SEEK_END);
			fsize = ftell(ifile);
			fseek(ifile, 0L, SEEK_SET);
			#ifdef GS_DEBUG
				printf("Size: %i\nAt %i\n", fsize, ftell(ifile));
			#endif
			
			c = 0; y = 0; ps = 0; pl = 0;
			if(fsize > 0)
			{
				do
				{
					if(SERVER_PANIC != 0)
					{
						printlog("[gs_listenSocket] Server panic! Closing connection %u", client_sock);
						goto con_end;
					}
					c = fread((void*)outBuf, 1, 8191, ifile);
					
					#ifdef GS_DEBUG
						printf("Read %i bytes (%i)\n", c, errno);
					#endif

					if(c > 0)
					{
						write_size = send(client_sock, outBuf, c, 0);
						#ifdef GS_DEBUG
							printf("Written %i bytes out of %i.\n", write_size, c);
						#endif
					}
					y += c;
				}while(y < fsize && c > 0);
				printlog("[gs_listenSocket] Sent %i bytes from %s to client %i", y, fileName, client_sock);
			}
			fclose(ifile);
			#ifdef GS_DEBUG
				printf("Done\n");
			#endif
		break;
		case 2:
			// Binary file. Just serve it.
			#ifdef GS_DEBUG
				printf("Binary file\n");
			#endif
			ifile = fopen(fileName, "rb");
			if(!ifile)
			{
				printlog("[gs_listenSocket] Could not open file %s (%i)\n", fileName, errno);
				write_size = send(client_sock, GSR_NOTAVAIL, strlen(GSR_NOTAVAIL), 0);
				goto con_end;
			}
			fseek(ifile, 0L, SEEK_END);
			fsize = ftell(ifile);
			fseek(ifile, 0L, SEEK_SET);
			#ifdef GS_DEBUG
				printf("Size: %i\nAt %i\n", fsize, ftell(ifile));
			#endif
			c = 0; y = 0; ps = 0; pl = 0;
			if(fsize > 0)
			{
				do
				{
					if(SERVER_PANIC != 0)
					{
						printlog("[gs_listenSocket] Server panic! Closing connection %u", client_sock);
						goto con_end;
					}
					c = fread((void*)outBuf, 1, 8191, ifile);
					#ifdef GS_DEBUG
						printf("Read %i bytes (%i)\n", c, errno);
					#endif
					if(c > 0)
					{
						write_size = send(client_sock, outBuf, c, 0);
						#ifdef GS_DEBUG
							printf("Written %i bytes out of %i.\n", write_size, c);
						#endif
					}
					y += c;
				}while(y < fsize && c > 0);
				printlog("[gs_listenSocket] Sent %i bytes from %s to client %i", y, fileName, client_sock);
			}
			fclose(ifile);
			#ifdef GS_DEBUG
				printf("Done\n");
			#endif
		break;
	}
con_end:
	shutdown(client_sock, 2);
	close(client_sock);
	mm_free(client_request);
	mm_free(client_message);
	mm_free(fileName);
	mm_free(outBuf);
	#ifdef GS_DEBUG
		printf("Connection ends\n");
	#endif
	printlog("[gs_listenSocket] Connection with %i is over.", client_sock);
	pthread_exit(0);
}
