### Setting up a basic Gopher Server

I recently wrote a small file server for the ancient protocol Gopher following the old RFC 1436 standard from 1993. The software only works in GNU Linux for now, and should be compilable in any system that has libpthread on it. So far, there is nothing really modern on it. You can find the code at [https://github.com/Senjihakku/gophserv](https://github.com/Senjihakku/gophserv) and it's under a GPL-3 license.

Eventually I'll make POSIX threads optional.

To install it in your system, first clone and compile the project. If you have git installed, you can accomplish this by using the git command

```
git clone https://github.com/Senjihakku/gophserv.git
cd gophserv
```

There is no configuration script or anything fancy so the normal make && sudo make install commands will work. If there are no issues with the compilation, it will install both gophserv and the man page in your system.

Before running the server, edit the configuration file gophserv.conf, located in the directory /etc/gophserv:

```
# vim /etc/gophserv/gophserv.conf
```

Now you can edit this configuration file with your favourite editor, which might be vi(m), nano, kate, or whatever. Each option is commented so no further instructions are required for this.

The public directory is located at /var/gophserv-www, and it contains all the data to be served by the server.

To start the server, just run gophserv.

![Gopher server running](https://preview.ibb.co/gsJyLH/Screenshot_20180319_000459.png "Gopher server running")

Now for the last test, try loading your directory root with Lynx. For instance:

```
$ lynx gopher://127.0.0.1:70
```

Just change that IP:PORT for whatever IP and PORT you are making the server listen to (If bindtoaddress is 0, you can connect to localhost). If you are greeted with a menu

![Gopher main menu](https://preview.ibb.co/i5QTLH/Screenshot_20180319_001003.png "Gopher main menu")

Congratulations! Now you can start editing your gophermaps and add your content to your liking. Enjoy bringing back the 90's! 
