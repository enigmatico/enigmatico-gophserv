#include "gophserv.h"
#include "utils.h"
#include "socket.h"
#include "fileserver.h"
#include "config.h"

int getType(char* ext)
{
	if(!strcmp(ext,"txt") || !strcmp(ext, "js") || !strcmp(ext, "css"))
	{
		return GOPHER_TEXT;
	}
	if(!strcmp(ext,"gif"))
	{
		return GOPHER_GIF;
	}
	if(!strcmp(ext,"jpg") || !strcmp(ext, "jpeg") || !strcmp(ext, "png") || !strcmp(ext, "bmp"))
	{
		return GOPHER_IMAGE;
	}
	if(!strcmp(ext,"com") || !strcmp(ext,"exe"))
	{
		return GOPHER_DOS;
	}
	if(!strcmp(ext,"wav") || !strcmp(ext,"mp3") || !strcmp(ext,"ogg"))
	{
		return GOPHER_SOUND;
	}
	if(!strcmp(ext,"hqx"))
	{
		return GOPHER_BINHEX;
	}
	if(!strcmp(ext,"html") || !strcmp(ext,"htm"))
	{
		return GOPHER_HTML;
	}

	return GOPHER_BIN;
}

int getFileType(char* ext)
{
	/* These are a bit redundant but for code clarity, it helps */
	if(!strcmp(ext,"txt") || !strcmp(ext, "js") || !strcmp(ext, "css")  || !strcmp(ext, "html") || !strcmp(ext, "htm"))
	{
		return GOPHER_T_TEXT;
	}
	if(!strcmp(ext,"gif"))
	{
		return GOPHER_T_BINARY;
	}
	if(!strcmp(ext,"jpg") || !strcmp(ext, "jpeg") || !strcmp(ext, "png") || !strcmp(ext, "bmp"))
	{
		return GOPHER_T_BINARY;
	}
	if(!strcmp(ext,"swf") || !strcmp(ext,"jar"))
	{
		return GOPHER_T_BINARY;
	}
	if(!strcmp(ext,"com") || !strcmp(ext,"exe"))
	{
		return GOPHER_T_BINARY;
	}
	if(!strcmp(ext,"wav") || !strcmp(ext,"mp3") || !strcmp(ext,"ogg"))
	{
		return GOPHER_T_BINARY;
	}
	if(!strcmp(ext,"avi") || !strcmp(ext,"mp4") || !strcmp(ext,"webm") || !strcmp(ext,"mov"))
	{
		return GOPHER_T_BINARY;
	}
	//Is this even in use at this day and age?
	if(!strcmp(ext,"hqx"))
	{
		return GOPHER_T_TEXT;
	}

	return GOPHER_T_BINARY;
}

// Make a directory listing
char* gs_directoryList(char* path)
{
	char *line = (char*)mm_request(512, NULL);
	char *fullPath = (char*)mm_request(4096, NULL);
	int fullPathLength = 0, strptr = 0;
	DIR *dp;
	struct dirent *ep; 
	//char line[512] = {0x0};
	char *gs_directoryList = NULL;
	char* ext = NULL;
	char type = 0x0L;
	struct stat path_stat;
	
	if(!serverConfig.cf_dirlist)
	{
		#ifdef GS_DEBUG
			printf("Directory listing is disallowed.\n");
		#endif
		gs_directoryList = (char*)mm_request(21, NULL);
		sprintf(gs_directoryList, "3Access denied\r\n.\r\n");
		mm_free(line);
		mm_free(fullPath);
		return gs_directoryList;
	}
	
	gs_directoryList = (char*)mm_request(6, NULL);
	//char fullPath[4096] = { 0x0 };

	fullPathLength = sprintf(fullPath, "%s%s", serverConfig.cf_public, path);

	if(fullPathLength < 0)
	{
		#ifdef GS_DEBUG
			printf("Error listing directory %s%s\n", serverConfig.cf_public, path);
		#endif
		mm_free(line);
		mm_free(fullPath);
		return NULL;
	}
    
	dp = opendir (fullPath);
	if (dp != NULL)
	{
		while (ep = readdir(dp))
		{
			if(!ep->d_name)
			{
				continue;
			}
			if(ep->d_name[0] == '.')
			{
				#ifdef GS_DEBUG
					printf("%s is a hidden file, skipping...\n", ep->d_name);
				#endif
				continue;
			}

			if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0)
			{
				continue;
			}

			ext = getPathExtension(ep->d_name, strlen(ep->d_name));
			type = GOPHER_BIN;
			
			if(ext)
			{
				type = getType(ext);
			}else{
				type = GOPHER_MENU;
				memset(&path_stat, 0x0, sizeof(struct stat));
				stat(path, &path_stat);
				if(S_ISREG(path_stat.st_mode))
				{
					type = GOPHER_BIN;
				}
			}
			sprintf(line, "%c%s\t%s/%s\t%s\t%i\r\n", type, ep->d_name, path, ep->d_name, (serverConfig.cf_usedomain)?serverConfig.cf_domain:serverConfig.cf_address, serverConfig.cf_port);
			gs_directoryList = mm_change_bsize(gs_directoryList, strptr+strlen(line)+6);
			strptr += sprintf(gs_directoryList + strptr, "%s", line);
			memset(line, 0x0, 512);
			mm_free(ext);
		}

		sprintf(gs_directoryList + strptr, "\r\n.\r\n");

		#ifdef GS_DEBUG
			printHex(gs_directoryList, strlen(gs_directoryList));
		#endif

		(void) closedir (dp);
	}
	else
	{
		#ifdef GS_DEBUG
			printf("Couldn't open the directory %s\n", fullPath);
		#endif
		mm_free(line);
		mm_free(fullPath);
		return NULL;
	}
	mm_free(line);
	mm_free(fullPath);
	return gs_directoryList;
}
